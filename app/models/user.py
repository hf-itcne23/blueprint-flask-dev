from apiflask import Schema
from apiflask.fields import Integer, String
from apiflask.validators import Length, OneOf

from app.extensions import db

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(128), unique=True, nullable=False)
    email = db.Column(db.String(128), unique=True, nullable=False)
    password = db.Column(db.String(1256), nullable=False)
    roles = db.relationship('UserRole', back_populates='user', cascade='all, delete-orphan')
    permissions = db.relationship('UserPermission', back_populates='user', cascade='all, delete-orphan')

    def __repr__(self):
        return f'<User {self.username}>'
    
    def to_dict(self):
        return {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'roles': [role.role.to_dict() for role in self.roles],
            'permissions': [permission.permission.to_dict() for permission in self.permissions]
        }
    
    def from_dict(self, data):
        for field in ['username', 'email', 'password']:
            if field in data:
                setattr(self, field, data[field])

class UserIn(Schema):
    username = String(required=True, validate=Length(min=1, max=128))
    email = String(required=True, validate=Length(min=1, max=128))
    password = String(required=True, validate=Length(min=1, max=1256))

class UserOut(Schema):
    id = Integer()
    username = String(validate=Length(min=1, max=128))
    email = String(validate=Length(min=1, max=128))
    roles = [RoleOut]
    permissions = [PermissionOut]

