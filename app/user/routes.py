from app.user import bp
from app.extensions import db
from app.models.user import User, UserIn, UserOut
from app.models.role import Role, RoleIn, RoleOut
from app.models.user_role import UserRole, UserRoleIn
from app.models.permission import Permission, PermissionIn, PermissionOut
from app.models.role_permission import RolePermission, RolePermissionIn
from app.models.user_permission import UserPermission, UserPermissionIn
from app.models.user_role import UserRole, UserRoleIn
from app.models.user_role import UserRoleOut
from app.models.role_permission import RolePermissionOut

@bp.get('/users')
def list_users():
    users = User.query.all()
    return UserOut.jsonify(users, many=True)

@bp.post('/users')
def create_user(user: UserIn):
    user = User(**user)
    db.session.add(user)
    db.session.commit()
    return UserOut.jsonify(user), 201

@bp.get('/users/<int:user_id>')
def get_user(user_id: int):
    user = User.query.get_or_404(user_id)
    return UserOut.jsonify(user)

@bp.put('/users/<int:user_id>')
def update_user(user_id: int, user: UserIn):
    user = User.query.get_or_404(user_id)
    user.update(user)
    db.session.commit()
    return UserOut.jsonify(user)

@bp.delete('/users/<int:user_id>')
def delete_user(user_id: int):
    user = User.query.get_or_404(user_id)
    db.session.delete(user)
    db.session.commit()
    return '', 204


