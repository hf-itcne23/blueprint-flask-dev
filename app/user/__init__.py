from apiflask import APIBlueprint

bp = APIBlueprint('user', __name__)

from app.user import routes