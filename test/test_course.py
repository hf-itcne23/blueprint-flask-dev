from test import client

def test_get_courses(client):
    response = client.get("/courses/")
    assert response.json[0]['title'] == 'M231 Security'

def test_post_course(client):
    response = client.post("/courses/", json={
            'title': 'M444 Math'
        })
    assert response.status_code == 201

